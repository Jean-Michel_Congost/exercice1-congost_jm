import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;


/**
 * Cette classe contient le programme "main" permettant ainsi de lancer l'application avec toutes les classes et méthodes associées.
 * On insère également dans cette classe une méthode permettant de visualiser
 * le codage en base 64 de l'image. 
 * @author Jean-Michel Congost
 *
 */
public class Exercice_1 {

	/**
	 * La méthode "main" permet de lancer l'application avec toutes les classes et méthodes associées.
	 * @param args est un argument permettant de renseigner la méthode principale "main"
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			File image = new File("petite_image.png");
			ImageSerializer serializer = new ImageSerializerBase64Impl();

			// Sérialization
			String encodedImage = (String) serializer.serialize(image);
			System.out.println(splitDisplay(encodedImage, 76));

			// Désérialisation
			byte[] deserializedImage = (byte[]) serializer.deserialize(encodedImage);

			// Vérifications
			// 1/ Automatique
			assert (Arrays.equals(deserializedImage, Files.readAllBytes(image.toPath())));
			System.out.println("Cette sérialisation est bien réversible :)");
			// 2/ Manuelle
			File extractedImage = new File("petite_image_extraite.png");
			new FileOutputStream(extractedImage).write(deserializedImage);
			System.out.println(
					"Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}// Fin main

	/**
	 * Méthode utile pour afficher une image sérialisée
	 *
	 * @param str est une chaîne de caractères
	 * @param chars est le nombre de caractères affichés en colonne
	 * @return retourne les caractères contenus dans l'argument "str"
	 */
	private static String splitDisplay(String str, int chars) {
		StringBuffer strBuf = new StringBuffer();
		int i = 0;
		strBuf.append("================== Affichage de l'image encodée en Base64 ==================\n");
		for (; i + chars < str.length();) {
			strBuf.append(str.substring(i, i += chars));
			strBuf.append("\n");
		}
		strBuf.append(str.substring(i));
		strBuf.append("\n================================== FIN =====================================\n");

		return strBuf.toString();
	}

}
