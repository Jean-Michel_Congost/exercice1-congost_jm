import java.io.File;

/**
 * Cette classe est de type Interface et liste l'ensemble des méthodes
 * pouvant être utilisées dans l'application.
 * @author Jean-Michel Congost
 *
 */

public interface ImageSerializer {

	String serialize(File image);

	byte[] deserialize(String encodedImage);
	

}
