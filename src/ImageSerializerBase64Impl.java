import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

/**
 * Cette classe définit les méthodes de codage et décodage en base 64.
 * @author Jean-Michel Congost
 *
 */
public class ImageSerializerBase64Impl implements ImageSerializer {

	/**
	 * Cette méthode permet de coder un fichier reçu en paramètre en base 64.
	 * @param image est un fichier image de type png ou jpeg par exemple.
	 */
	@Override
	public String serialize(File image) {

		// Declaration de la variable String à retourner en fin de méthode
		String stringEncode = null;
		try {
			// Je mets dans un "Stream" l'image reçue en paramètre
			FileInputStream fichierEnEntree = new FileInputStream(image);
			// Je crée un tableau de bytes de taille = à la longueur de l'image
			byte[] bytes = new byte[(int) image.length()];
			// Je lis le tableau de bytes
			fichierEnEntree.read(bytes);
			// J'encode le tableau "bytes" en base64 et l'affecte à la variable
			// "stringEncode"
			stringEncode = Base64.getEncoder().encodeToString(bytes);
			// Je ferme le fichier "fichierEnEntree"
			fichierEnEntree.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return stringEncode;
	}

	/**
	 * Cette méthode permet de décoder une chaîne de caractères codée en base 64.
	 * @param encodedImage est une chaîne de caractères codée en base 64.
	 */
	@Override
	public byte[] deserialize(String encodedImage) {

		// Je crée un tableau de bytes de longueur de la String encodedImage
		byte[] bytes = new byte[(int) encodedImage.length()];
		//Je décode le fichier encodedImage pour le remetre dans son format initial
		bytes = Base64.getDecoder().decode(encodedImage) ;

		return bytes;
	}

}
